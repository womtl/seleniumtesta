package dz;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class TestPoiskLongsliva {
    protected WebDriver driver;

    @Test
    public void TestA() {
        SoftAssertions softAssertions = new SoftAssertions();
        WebDriver driver = new ChromeDriver();
        driver.get("https://homebrandofficial.ru/wear");
        WebElement zadershka1 = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='js-store-search-mob-btn t-store__filter__search-mob-btn t-descr t-descr_xs']")));
        driver.findElement(By.xpath("//*[@class='js-store-search-mob-btn t-store__filter__search-mob-btn t-descr t-descr_xs']")).click();
        driver.findElement(By.xpath("//*[@class='t-store__filter__input js-store-filter-search']")).sendKeys("Лонгслив White&Green");
        driver.findElement(By.xpath("//*[@class='t-store__search-icon js-store-filter-search-btn']")).click();
        WebElement zadershka2 = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='t-store__filter__prods-number js-store-filters-prodsnumber-wrap t-descr t-descr_xxs']")));

        String count = driver.findElement(By.xpath("//*[@class='t-store__filter__prods-number js-store-filters-prodsnumber-wrap t-descr t-descr_xxs']")).getText();
        String expectedCount = "Найдено: 1";
        softAssertions.assertThat(count).as("Неправильное количество").isEqualTo(expectedCount);

        String name = driver.findElement(By.xpath("//*[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md']")).getText();
        String expectedName = "Лонгслив White&Green";
        softAssertions.assertThat(name).as("Неправильное название").isEqualTo(expectedName);

        String cost = driver.findElement(By.xpath("//*[@class='js-product-price js-store-prod-price-val t-store__card__price-value notranslate']")).getText();
        String expectedCost = "2 800";
        softAssertions.assertThat(cost).as("Неправильная цена").isEqualTo(expectedCost);

        softAssertions.assertAll();


    }
}